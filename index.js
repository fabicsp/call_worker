const { Worker } = require("worker_threads");

function callWorker({ moduleName, pathToModule, params }) {
  const pathToWorker = `${__dirname}/worker/otherThread.js`;

  return new Promise((res, rej) => {
    const worker = new Worker(pathToWorker, {
      workerData: {
        moduleName,
        pathToModule,
        params,
      },
    });

    worker.on("message", (msg) => {
      res(msg);
    });
    worker.on("error", (err) => {
      rej(err);
    });
  });
}

module.exports = {
  callWorker: callWorker,
};
