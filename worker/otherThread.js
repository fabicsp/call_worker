const { parentPort, workerData } = require("worker_threads");
const { moduleName, pathToModule, params } = workerData;

const { [moduleName]: requiredModule } = require(pathToModule);

(async () => {
  try {
    const msg = await requiredModule(params);
    parentPort.postMessage(msg);
  } catch (error) {
    parentPort.postMessage(error);
  }
})();
